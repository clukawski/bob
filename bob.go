package main

import (
    "os/exec"
    "fmt"
    "strings"
    "log"
)

func main() {
    gopath, err := exec.LookPath("go")
    if err != nil {
        log.Fatal("golang binary missing")
    }

    gitpath, err := exec.LookPath("git")
    if err != nil {
        log.Fatal("git binary missing")
    }

    out, err := exec.Command(gitpath, "pull").Output()
    fmt.Printf("%s", out)

    if (strings.Contains(string(out[:]), "remote")) {
        out, err = exec.Command(gopath, "build").CombinedOutput()

        fmt.Printf("%s", out)
    }
}
